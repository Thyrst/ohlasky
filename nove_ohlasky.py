import os
import sys
import re
import logging
from datetime import datetime, timedelta
from getpass import getpass
from grab import Grab
from lxml import etree
from pprint import PrettyPrinter

#logging.basicConfig(level=logging.DEBUG)

FILENAME = '/tmp/moutnice_ohlasky'
DOWNLOAD_FILENAME = FILENAME + '.doc'
WORK_FILENAME = FILENAME + '.html'

URL_LOGIN = 'https://moutnice.farnost.cz/tajne/?odhl='
URL_DATA = 'https://moutnice.farnost.cz/tajne/ohlasky_n.php'

grab = Grab(connect_timeout=10)

password = getpass()

login = {
    'login': 'Takerian',
    'passw': password,
}

grab.go(URL_LOGIN, post=login)

tree = grab.doc.tree
title = tree.xpath('//h1/text()')
if title and title[0] == 'Vítejte v administraci stránek www.Moutnice.Farnost.cz.':
    print('Logged succesfully', flush=True)
else:
    logging.debug('%r', title)
    sys.exit('Bad password.')


try:
    url = sys.argv[1]
except IndexError:
    url = input('URL: ')

#if os.path.isfile(DOWNLOAD_FILENAME):
#    os.remove(DOWNLOAD_FILENAME)

os.system('wget -O {} "{}" && soffice --convert-to html --outdir /tmp/ {}'.format(DOWNLOAD_FILENAME, url, DOWNLOAD_FILENAME))

tree = etree.parse(WORK_FILENAME, etree.HTMLParser())

data = {}
day = -1
the_day = datetime.today()
year = the_day.year

def set_date(date):
    global the_day

    key = 'a' + str(day)
    try:
        date = re.search(r'\d+\.\d+\.', date)
        date = date.group().split('.')
        date = datetime(year, int(date[1]), int(date[0]))
    except (AttributeError, IndexError):
        pass
    else:
        the_day = date

    data[key] = the_day.strftime("%m/%d/%Y")

def set_title(title):
    key = 'b' + str(day)
    title = ' '.join(title.split())
    data[key] = title

def set_hour(hour):
    key = 'c' + str(day)
    hour = hour.strip()
    if data.get(key) and hour:
        data[key] += ', ' + hour
    elif data.get(key) is None:
        data[key] = hour

def set_intentions(intentions):
    key = 'd' + str(day)
    intentions = intentions.strip()
    if data.get(key) and intentions:
        data[key] += '; ' + intentions
    elif data.get(key) is None:
        data[key] = intentions

rows = tree.xpath('//table/tr')
del(rows[1]) # delete first saturday
del(rows[0]) # delete header
childs = 0

for row in rows:
    if childs == 0:
        day += 1

        rowspan = row.xpath('td[1]/@rowspan')
        if rowspan:
            childs = int(rowspan[0]) - 1

        date = ''.join(row.xpath('td[1]/descendant-or-self::*/text()'))
        set_date(date)
        the_day += timedelta(days=1)

        title = ''.join(row.xpath('td[2]/descendant-or-self::*/text()'))
        set_title(title)

        hour = ''.join(row.xpath('td[3]/descendant-or-self::*/text()'))
        set_hour(hour)

        intentions = ''.join(row.xpath('td[4]/descendant-or-self::*/text()'))
        set_intentions(intentions)

    else:
        childs -= 1

        hour = ''.join(row.xpath('td[1]/descendant-or-self::*/text()'))
        set_hour(hour)

        intentions = ''.join(row.xpath('td[2]/descendant-or-self::*/text()'))
        set_intentions(intentions)

text = tree.xpath('//body/p/descendant-or-self::*/text()')
text = ''.join(node.upper() for node in text[4:])
data['text'] = text.lstrip().replace('\n\n', '<br>')

printer = PrettyPrinter(indent=4)
printer.pprint(data)

permission = input('Is it OK? [Y/n] ')
if permission.lower() == 'n':
    sys.exit('Aborting.')
else:
    response = grab.go(URL_DATA, post=data, charset='utf-8')
    print('Response status code: ' + str(response.code))
    print('Check result: http://moutnice.farnost.cz/?id=ohlasky_n1.php')
